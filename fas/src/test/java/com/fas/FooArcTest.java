package com.fas;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.lang.ArchRule;

import org.junit.Test;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;
import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

public class FooArcTest {
  JavaClasses importedClasses = new ClassFileImporter().importPackages("com.fas");

  @Test
  public void verificarDependenciasParaCamadaPersistencia() {
    ArchRule rule = classes()
      .that().resideInAPackage("..persistence..")
      .should().onlyHaveDependentClassesThat().resideInAnyPackage("..persistence..", "..service..");

      rule.check(importedClasses);
  }

  //Nenhuma classe dentro da camada de persistencia pode depender da camada de service
  @Test
  public void verificarDependenciasDaCamadaPersistencia() {
    ArchRule rule = noClasses()
      .that().resideInAPackage("..persistence..")
      .should().dependOnClassesThat().resideInAnyPackage("..service..");

      rule.check(importedClasses);
  }
}